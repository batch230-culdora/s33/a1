/*S33 Activity:
1. In the S33 folder, create an activity folder and an index.html and a script.js file inside of it.
2. Link the index.js file to the index.html file.
13. Create a git repository named S33.
14. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
15. Add the link in Boodle.
*/



// 3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.
// 4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.

async function fetchData(){

   await fetch('https://jsonplaceholder.typicode.com/todos')
		.then((response) => response.json())
		.then((json) => {

	let list = json.map((todos => {
		return todos.title;
	}))

		console.log(list);

	});


// 5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
// 6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.

	await fetch("https://jsonplaceholder.typicode.com/todos/1")
		.then(res => res.json())
		.then(response => console.log(response));

	await fetch("https://jsonplaceholder.typicode.com/todos/1")
		.then(response => response.json())
		.then(res => console.log(`The item "${res.title}" on the list has a status of ${res.completed}`))


// 7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.
	
	await fetch("https://jsonplaceholder.typicode.com/todos", 
		{
			method: "POST",
			headers: {
				"Content-Type" : "application/json"
					},
		body: JSON.stringify({
			title: "Created to Do List Item",
			userId: 1
		})
	})
	.then(response => response.json())
	.then(json => console.log(json))


// 8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
// 9. Update a to do list item by changing the data structure to contain the following properties:
// - Title
// - Description
// - Status
// - Date Completed
// - User ID



	await fetch("https://jsonplaceholder.typicode.com/todos/1", 
		{
			method: "PUT",
			headers: {
				"Content-Type" : "application/json"
					},
		body: JSON.stringify({
			dateCompleted: "Pending",
			description: "To update the my to do list with a different data structure",
			status: "Pending",
			title: "Updated To Do List Item",
			userId: 1
		})
	})
	.then(response => response.json())
	.then(json => console.log(json))


// 10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.
// 11. Update a to do list item by changing the status to complete and add a date when the status was changed.

	await fetch("https://jsonplaceholder.typicode.com/todos/1", 
		{
    		method: "PATCH",
    		headers: {
        		"Content-type" : "application/json"
    	},
    	body: JSON.stringify({
        	status: "Complete",
        	dataCompleted: "07/09/21",
   		})
	})
	.then(response => response.json())
	.then(json => console.log(json))


// 12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.

	await fetch("https://jsonplaceholder.typicode.com/todos/1", 
		{
			method: "DELETE"
		}
	)
	.then(response => response.json())
	.then(json => console.log(json))


}
fetchData();